using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public float damage = 10f;                  // Da�o que inflige el arma
    public float range = 100f;                  // Rango m�ximo del Raycast
    public Camera fpsCam;                       // Referencia a la c�mara del jugador
    public ParticleSystem muzzleFlash;          // Efecto de fogonazo del arma


    public int balasPorCartucho = 10;           // Cantidad de balas por cartucho
    public int cartuchosDisponibles = 5;        // Cantidad de cartuchos disponibles

    private int balasRestantes;                 // Cantidad de balas restantes en el cartucho actual

   private bool recargando=false;

    private void Start()
    {
       

        balasRestantes = balasPorCartucho;
    }
    private void Update()
    {
        GameObject jugador = GameObject.Find("jugador");
        InterfazBalas interfazBalas = jugador.GetComponent<InterfazBalas>();
      
        if (Input.GetButtonDown("Fire1"))
        {
            if (balasRestantes > 0 && cartuchosDisponibles > 0)
            {
                DispararArma();
                balasRestantes--;
               


            }
            else if (   (balasRestantes == 0 && cartuchosDisponibles > 0) )
            {
                Recargar();
            }
        }

        //actualizar interfaz
        interfazBalas.ActualizarInterfazBalas(balasRestantes, cartuchosDisponibles);
    }


    void DispararArma()
    {
        // Mostramos el fogonazo del arma
        muzzleFlash.Play();

        // Creamos el Raycast y comprobamos si golpeamos algo
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            // Comprobamos si hemos golpeado algo con un script "Da�able"
            Da�able target = hit.transform.GetComponent<Da�able>();
            if (target != null)
            {
                // Si hemos golpeado algo con el script "Da�able", le hacemos da�o
                target.RecibirDa�o(damage);
            }
        }
    }

    void Recargar()
    {
        if (cartuchosDisponibles > 0)
        {
            cartuchosDisponibles--;
            balasRestantes = balasPorCartucho;
        }
    }

  
}

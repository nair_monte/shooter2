using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ControladorJugador : MonoBehaviour

{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 10f;
    public float smoothTime = 0.1f;
    private Rigidbody rb;
    private Vector3 moveDirection;

    private Vector3 currentVelocity = Vector3.zero;

    private bool isGrounded;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // Verificar si el jugador est� en el suelo
        if (isGrounded)
        {
            // Verificar si el jugador quiere saltar
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
            }
        }
    }

    private void FixedUpdate()
    {
        // Obtener entrada horizontal y vertical
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveZ = Input.GetAxisRaw("Vertical");

        // Crear un vector de movimiento a lo largo del eje horizontal y vertical
        moveDirection = new Vector3(moveX, 0f, moveZ).normalized;

        //// Aplicar movimiento a trav�s de la fuerza en el Rigidbody RIGIDO
        //rb.MovePosition(rb.position + transform.TransformDirection(moveDirection) * moveSpeed * Time.fixedDeltaTime);


        rb.MovePosition(rb.position + Vector3.SmoothDamp(rb.velocity, transform.TransformDirection(moveDirection) * moveSpeed, ref currentVelocity, smoothTime) * Time.fixedDeltaTime);


    }

    private void OnCollisionEnter(Collision other)
    {
        // Verificar si el jugador est� en el suelo
        if (other.gameObject.CompareTag("Piso"))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        // Verificar si el jugador ya no est� en el suelo
        if (other.gameObject.CompareTag("Piso"))
        {
            isGrounded = false;
        }
    }
}


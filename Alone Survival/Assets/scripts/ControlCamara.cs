using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour

{
    [SerializeField] private float mouseSensitivity = 100f;

    public Transform jugador;

    float xRotation = 0f;

    void Start()
    {
        // Ocultar el cursor y desactivar su movimiento
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        // Obtener el movimiento del mouse
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        // Rotar la c�mara en el eje X
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        // Rotar al jugador en el eje Y
        jugador.Rotate(Vector3.up * mouseX);
    }
}

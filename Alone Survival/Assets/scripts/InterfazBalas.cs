using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfazBalas : MonoBehaviour
{
    public TMPro.TMP_Text txtBalas;

    public void ActualizarInterfazBalas(int balas, int cartuchos)
    {
        
       
            txtBalas.text = $"{balas} / {cartuchos - 1}";
       
    }
}

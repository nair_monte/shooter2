using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour

{
    public Transform player;
    public float stoppingDistance = 2f;
    private NavMeshAgent navMeshAgent;

    bool isKinematicEnabled = true;
    //private Animator animator;

    Rigidbody rigidbody;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        //  animator = GetComponent<Animator>();

        rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);
        if (distanceToPlayer <= stoppingDistance)
        {
            navMeshAgent.isStopped = true;
           // animator.SetTrigger("Attack"); // activa la animación de ataque
        }
        else
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(player.position);
           // animator.ResetTrigger("Attack"); // resetea el trigger para la animación de ataque
        }


        //si se mueve es kinematico si no, no
        if (navMeshAgent.velocity.magnitude > 0)
        {
            isKinematicEnabled = false;
        }
        else
        {
            isKinematicEnabled = true;
        }
    }


    private void FixedUpdate()
    {
        if (isKinematicEnabled)
        {
            rigidbody.isKinematic = true;
        }
        else
        {
            rigidbody.isKinematic = false;
        }
    }

}




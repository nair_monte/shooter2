using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlArma : MonoBehaviour
{
    Animator animator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }


    void Update()
    {
        // Verifica si el bot�n derecho del mouse est� siendo presionado
        if (Input.GetButton("Fire2"))
        {
            // Establece el bool "Apuntando" en true
            animator.SetBool("Apuntando", true);
        }
        else
        {
            // Establece el bool "Apuntando" en false
            animator.SetBool("Apuntando", false);
        }
    }
}